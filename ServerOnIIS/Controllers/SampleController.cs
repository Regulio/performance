﻿using System.Threading.Tasks;
using System.Web.Http;

namespace ServerOnIIS.Controllers
{
    public class SampleController : ApiController
    {
        [Route("sample")]
        [HttpPost]
        public async Task<IHttpActionResult> Post()
        {
            await Task.Delay(500);
            return Ok(new { Some = "Value", Other = 15 });
        }
    }
}