﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using Timer = System.Timers.Timer;

namespace Client
{
    internal class Program
    {
        static readonly ConcurrentQueue<Message> InputQueue = new ConcurrentQueue<Message>();

        static int _received;
        static int _processed;
        static int _failed;

        public static void Main(string[] args)
        {
            ServicePointManager.DefaultConnectionLimit = 2000;
            const int ratePerSecond = 1500;

            var timer = new Timer(1000);
            timer.Elapsed += (sender, eventArgs) =>
            {
                for (var i = 1; i <= ratePerSecond; i++)
                {
                    InputQueue.Enqueue(new Message(Guid.NewGuid()));
                    _received++;
                }
                Console.WriteLine($"Messages received: {_received}");
                Console.WriteLine($"Messages processed: {_processed}, failed: {_failed}");
            };
            timer.Start();
            new Thread(Process).Start();

            Console.ReadLine();
        }

        public static void Process()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            while (true)
            {
                Message message;
                if (InputQueue.TryDequeue(out message)) Request(client, message);
            }
        }

        public static async void Request(HttpClient client, Message message)
        {
            try
            {
                var request = new Request(message.Payload);
                await client.PostAsync("http://localhost/server/sample", new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json"));
                Interlocked.Increment(ref _processed);
            }
            catch (Exception)
            {
                Interlocked.Increment(ref _failed);
            }
        }
    }

    public class Message
    {
        public readonly Guid Payload;

        public Message(Guid payload)
        {
            Payload = payload;
        }
    }

    public class Request
    {
        public readonly Guid Payload;

        public Request(Guid payload)
        {
            Payload = payload;
        }
    }
}