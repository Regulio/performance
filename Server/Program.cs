﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin.Hosting;
using Owin;

namespace Server
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            const string baseAddress = "http://wf1890/server/";
            using(WebApp.Start<Startup>(baseAddress))
            {
                Console.WriteLine($"Started webserver on {baseAddress}, press [Enter] to exit.");
                Console.ReadLine();
            }
        }
    }

    internal class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            appBuilder.UseWebApi(config);
        }
    }

    public class SampleController : ApiController
    {
        public async Task<IHttpActionResult> Post()
        {
            await Task.Delay(500);
            return Ok(new {Some = "Value", Other = 15});
        }
    }
}